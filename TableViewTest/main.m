//
//  main.m
//  TableViewTest
//
//  Created by Henrique Valadares on 29/06/17.
//  Copyright © 2017 Henrique Valadares. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
